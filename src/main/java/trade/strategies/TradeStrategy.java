package trade.strategies;

import apikunaio.contracts.response.Order;
import apikunaio.contracts.response.WalletBalance;
import trade.business.TradeController;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class TradeStrategy implements ITradeStrategy {

    private final TradeController tradeController;
    private final String tradePairSymbol;

    public TradeStrategy(TradeController tradeController) {
        this.tradeController = tradeController;
        this.tradePairSymbol = tradeController.tradePairSymbol;
    }

    public void execute(double amountOfCurrencyToOneBuyOrder, double desireProfitPercent, Map<Integer, LinkedList<Double>> tickerStorage, int currentTimepoint) {
        System.err.println("Currency amount for 1 buy operation: " + amountOfCurrencyToOneBuyOrder + tradePairSymbol.toUpperCase());
        System.err.println(new SimpleDateFormat("HH:mm:ss yyyy/MM/dd ").format(new Date()));
        System.err.println("The current price for " + tradePairSymbol.toUpperCase() + " - " + tradeController.getCurrentPrice(tradePairSymbol));

        tradeController.reSetupTradeLimit();

        collectMarketPrice(tickerStorage, currentTimepoint);

        if (isTimeToBuy(tickerStorage)) {
            if (!tradeController.isCurrencyLimitReachedOut(tradeController.tradeLimit_LowBorder)) {
                trade(amountOfCurrencyToOneBuyOrder, desireProfitPercent);
                cleanMarketPrices(tickerStorage);
            }
        }
    }

    protected void collectMarketPrice(Map<Integer, LinkedList<Double>> tickerStorage, int currentTimepoint) {
        double currentPrice = tradeController.getCurrentPrice(tradePairSymbol);

        // Add market price
        tickerStorage.forEach((key, value) -> {
            if (currentTimepoint % key == 0) {
                addPriceToStorage(value, currentPrice);
            }
        });

        // print saved price for 5th,15th,30th,60th minute
        tickerStorage.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(ticker ->
                System.out.println((10 % ticker.getKey() == 0 ?
                        " " + ticker.getKey() :
                        ticker.getKey()) + " min ticker: " + getPricesInTickerStorage(ticker.getValue())));
    }

    protected boolean isTimeToBuy(Map<Integer, LinkedList<Double>> tickerStorages) {
        List<Double> pricesForCreatingBuyOrder = new ArrayList<Double>();

        tickerStorages.entrySet().forEach(x -> {
            if (x.getValue().size() == 3) {
                if (x.getValue().get(0) > x.getValue().get(1) && x.getValue().get(1) > x.getValue().get(2)) {
                    pricesForCreatingBuyOrder.add(x.getValue().get(2));
                    System.err.println("Price for order:" + x.getValue().get(2));
                }
            }
        });

        List<Double> onePriceForCreatingBuyOrder = new ArrayList<Double>();
        try {
            onePriceForCreatingBuyOrder = pricesForCreatingBuyOrder.stream().distinct().collect(Collectors.toList());
        } catch (Exception e) {
            e.getStackTrace();
        }

        return onePriceForCreatingBuyOrder.size() > 0;
    }

    protected void trade(double amountOfCurrencyToOneBuyOrder, double desireProfitPercent) {
        double tradeMarketCommissionPercent = 0.2;
        try {
            Order buyOrder = tradeController.createBuyOrderByMarket(amountOfCurrencyToOneBuyOrder);

            Order executedOrder = tradeController.getExecutedOrderInfo(buyOrder.orderId);

            if (executedOrder != null) {

                WalletBalance cryptoWallet = tradeController.getWalletBalance(tradePairSymbol.substring(0, 3));

                if (cryptoWallet != null) {
                    tradeMarketCommissionPercent = 100.0 - ((cryptoWallet.availableFunds * 100.0) / buyOrder.initialOrderVolume);

                    double realBuyPrice = executedOrder.orderPrice == 0.0 ? executedOrder.avgOrderPrice : executedOrder.orderPrice;

                    Order sellOrder = tradeController.createSellOrderByLimit(cryptoWallet.availableFunds, realBuyPrice, tradeMarketCommissionPercent, desireProfitPercent);
                } else {
                    throw new Exception("Can't get [" + tradePairSymbol.substring(0, 3) + "] wallet info.");
                }

            } else {
                System.err.println("Wait for executing [" + buyOrder.orderId + "] and SELL it MANUALLY by price not lower " +
                        buyOrder.orderPrice + (buyOrder.orderPrice * (tradeMarketCommissionPercent * 2) / 100.0) + " + your profit %.");
            }
        } catch (Exception e) {
            System.err.println("Trade has been skipped.");
        }
    }

    private void addPriceToStorage(LinkedList<Double> priceStorage, double price) {
        if (priceStorage.size() >= 3) {
            priceStorage.remove(0);
        }
        priceStorage.add(price);
    }

    private void cleanMarketPrices(Map<Integer, LinkedList<Double>> priceStorage) {
        double maxValue = 9999999.99;

        priceStorage.forEach((key, value) -> {
            if (key == 5 || key == 15 || key == 30) {
                if (value.size() >= 3) value.set(2, 0.0);
                if (value.size() >= 2) value.set(1, 0.0);
                if (value.size() >= 1) value.set(0, 0.0);
            }

            if (key == 60)
                if (value.size() >= 1) value.set(0, 0.0);
            if (value.size() >= 2) value.set(1, 0.0);
        });
    }

    private String getPricesInTickerStorage(LinkedList<Double> stack) {
        StringBuilder priceStack = new StringBuilder();

        stack.forEach(x -> priceStack.append(x).append(" "));
        return priceStack.toString();
    }

}
