package trade.strategies;

import java.util.LinkedList;
import java.util.Map;

public interface ITradeStrategy {

    void execute(double currencyAmount, double desireProfitPercent, Map<Integer, LinkedList<Double>> tickerStorage, int currentTimepoint);
}
