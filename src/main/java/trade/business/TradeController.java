package trade.business;

import apikunaio.TradeControllerAPI;
import apikunaio.contracts.response.MarketPairInfo;
import apikunaio.contracts.response.Order;
import apikunaio.contracts.response.WalletBalance;

import java.util.ArrayList;

public class TradeController implements ITradeController {

    private final TradeControllerAPI apiController;
    public String tradePairSymbol;
    public double tradeLimit_LowBorder;
    public double amountOfCurrencyForTrade;

    public TradeController(double amountOfCurrencyForTrade, String tradePairSymbol, String publicAPIKey, String secretAPIKey) {
        apiController = new TradeControllerAPI(publicAPIKey, secretAPIKey);

        this.tradePairSymbol = tradePairSymbol.toLowerCase();
        this.amountOfCurrencyForTrade = amountOfCurrencyForTrade;

        checkFundsAvailability(amountOfCurrencyForTrade);
    }

    public void reSetupTradeLimit() {
        String currencySymbol = tradePairSymbol.substring(3, 6);
        double sumOfActiveOrders = 0.0;

        //get sum of all active orders
        ArrayList<Order> activeOrdersList = getActiveOrders(tradePairSymbol);
        if (activeOrdersList != null) {
            for (Order activeOrder : activeOrdersList) {
                sumOfActiveOrders += Math.round((activeOrder.orderVolume * activeOrder.orderPrice) * 100.0) / 100.0;
            }
            if (sumOfActiveOrders > 0.0) {
                System.out.println("Estimated profit of active orders: " + sumOfActiveOrders + currencySymbol.toUpperCase());
            }
        }

        WalletBalance wallet = getWalletBalance(currencySymbol);
        if (wallet != null) {

            double newPredictableBalance = getWalletBalance(currencySymbol).availableFunds + sumOfActiveOrders;

            tradeLimit_LowBorder = Math.round((newPredictableBalance - amountOfCurrencyForTrade) * 100.0) / 100.0;

            System.err.println("Low trade border has been changed to " + tradeLimit_LowBorder + currencySymbol.toUpperCase());
        }
        System.out.println();
    }

    public void checkFundsAvailability(double amountOfCurrencyForTrade) {
        String currencySymbol = tradePairSymbol.substring(3, 6).toUpperCase();

        WalletBalance wallet = getWalletBalance(currencySymbol);
        if (wallet == null) {
            System.err.println("Can't get [" + currencySymbol + "] balance.\n Try to check network.");
            System.exit(1);
        } else {
            System.out.println("Trade funds: " + amountOfCurrencyForTrade + currencySymbol +
                    "\nAvailable funds :" + wallet.availableFunds + currencySymbol);

            if (wallet.availableFunds > amountOfCurrencyForTrade) {
                tradeLimit_LowBorder = wallet.availableFunds - amountOfCurrencyForTrade;
                System.out.println("Low trade border (trade limit): " + tradeLimit_LowBorder + currencySymbol);
                System.err.println("Funds are enough for trade! ");
            } else {
                System.err.println("Not enough money to trade!");
                System.exit(1);
            }
        }
    }

    public boolean isOrderCompleted(int orderId) {
        boolean isOrderCompleted = false;
        int checkPauseInSeconds = 20;
        int checkAttempts = 6;

        Order order = null;
        int counter = 0;

        do {
            do {
                if (counter > 0) {
                    try {
                        Thread.sleep(checkPauseInSeconds * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                order = apiController.getExecutedOrderInfo(tradePairSymbol, orderId);
                counter++;
            } while (order == null && counter < checkAttempts);
        } while (!order.orderStatus.equals("EXECUTED") && counter < checkAttempts);

        if (order == null) {
            System.err.println(String.format(
                    "Order [%d] for trade pair [%s] was not executed withing [%d]!",
                    orderId, tradePairSymbol, checkPauseInSeconds * checkAttempts));
            isOrderCompleted = false;

        } else {
            System.err.println("Order [" + order.orderId + "] has been executed!" +
                    "\n\tOrder price:" + (order.orderPrice == 0.0 ? order.avgOrderPrice : order.orderPrice) +
                    "\n\tOrder volume:" + order.initialOrderVolume);
            isOrderCompleted = true;
        }

        return isOrderCompleted;
    }

    public boolean isCurrencyLimitReachedOut(double tradeLimitLevel) {
        boolean isLimitReachedOut = true;
        String symbol = tradePairSymbol.substring(3, 6).toUpperCase();

        WalletBalance wallet = getWalletBalance(tradePairSymbol.substring(3, 6));
        if (wallet != null) {
            if (wallet.availableFunds <= tradeLimitLevel) {
                isLimitReachedOut = true;
                System.err.println("The trade limit [" + symbol + "] has been reached out! ");
                System.out.print("Avail. funds:" + wallet.availableFunds + symbol +
                        " Trade limit: " + tradeLimitLevel + symbol);
            } else {
                isLimitReachedOut = false;
            }
        }

        return isLimitReachedOut;
    }

    public Double getCurrentPrice(String tradePairSymbol) {
        MarketPairInfo pairInfo = null;
        int checkPauseInSeconds = 10;
        int checkAttempts = 6;
        int counter = 0;
        do {
            if (counter > 0) {
                try {
                    Thread.sleep(checkPauseInSeconds * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            pairInfo = apiController.getMarketPairInfo(tradePairSymbol);
            counter++;
        } while (pairInfo == null && counter < checkAttempts);

        return pairInfo != null ? pairInfo.priceLast : null;
    }

    public WalletBalance getWalletBalance(String currencyCode) {
        WalletBalance wallet = null;
        try {
            wallet = apiController.getBalanceOf(currencyCode);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return wallet;
    }

    public Order createBuyOrderByMarket(double amountOfCurrency) {
        String orderType = "market";
        Order order = null;
        try {

            double amountOfCryptoCurrencyToBuy = amountOfCurrency / getCurrentPrice(tradePairSymbol);
            double roundedAmountOfCryptoCurrencyToBuy = Math.round(amountOfCryptoCurrencyToBuy * 100.0) / 100.0;

            order = apiController.postBuyOrder(tradePairSymbol, orderType, roundedAmountOfCryptoCurrencyToBuy);

            System.err.println("The BUY [" + orderType.toUpperCase() + "] type order was posted! " +
                    "\n\tCurrency amount: " + amountOfCurrency + tradePairSymbol.substring(3, 6).toUpperCase() +
                    "\n\tCrypto currency amount will be bought: " + roundedAmountOfCryptoCurrencyToBuy + tradePairSymbol.substring(0, 3).toUpperCase());
        } catch (Exception e) {
            e.getStackTrace();
        }

        return order;
    }

    /*
    0.25% - commission of market for each operation (0.25% on buy + 0.25% on sell = 0.50%) half of 1 percent
        // order price from createBuyOrderByMarket + 0.50% => profit = 0%
        // order price from createBuyOrderByMarket + 0.75% => profit = 0.25%
     */
    public Order createSellOrderByLimit(double amountOfCryptoCurrency, double priceOfBuy, double marketCommission, double percentOfDesireProfit) {
        String orderType = "limit";
        Order order = null;

        try {
            System.out.println("Try to create SELL order with data: amountOfCryptoCurrency:" + amountOfCryptoCurrency +
                    " priceOfBuy:" + priceOfBuy + " marketCommission:" + marketCommission + " percentOfDesireProfit:" + percentOfDesireProfit);

            double sellPrice = priceOfBuy + (priceOfBuy * ((marketCommission * 2.0) + percentOfDesireProfit) / 100.0);
            double roundedSellPrice = Math.round(sellPrice * 100.0) / 100.0;

            order = apiController.postSellOrder(tradePairSymbol, orderType, amountOfCryptoCurrency, roundedSellPrice);

            System.err.println("The " + tradePairSymbol.toUpperCase() + " SELL [" + orderType.toUpperCase() + "] type order was posted!" +
                    "\n\tBuy price was: " + priceOfBuy +
                    "\n\tSell price (including commissions and desirable profit): " + roundedSellPrice +
                    "\n\tCrypto currency amount will be sold: " + amountOfCryptoCurrency + tradePairSymbol.substring(0, 3).toUpperCase());
        } catch (Exception e) {
            e.getStackTrace();
        }

        return order;
    }

    public Order getExecutedOrderInfo(int orderId) {
        return isOrderCompleted(orderId) ? apiController.getExecutedOrderInfo(tradePairSymbol, orderId) : null;
    }

    public ArrayList<Order> getActiveOrders(String tradePairSymbol) {
        ArrayList<Order> orderslist = null;
        try {
            orderslist = apiController.getActiveOrders(tradePairSymbol);
        } catch (Exception e) {
            e.getStackTrace();
        }

        return orderslist;
    }
}
