package trade.business;

import apikunaio.contracts.response.Order;
import apikunaio.contracts.response.WalletBalance;

import java.util.ArrayList;

public interface ITradeController {

    void reSetupTradeLimit();

    void checkFundsAvailability(double amountOfCurrencyForTrade);

    boolean isOrderCompleted(int orderId);

    boolean isCurrencyLimitReachedOut(double tradeLimitLevel);

    Double getCurrentPrice(String tradePairSymbol);

    WalletBalance getWalletBalance(String currencyCode);

    Order createBuyOrderByMarket(double amountOfCurrency);

    Order createSellOrderByLimit(double amountOfCryptoCurrency, double priceOfBuy, double marketCommission, double desireProfitPercent);

    Order getExecutedOrderInfo(int orderId);

    ArrayList<Order> getActiveOrders(String tradePairSymbol);

}
