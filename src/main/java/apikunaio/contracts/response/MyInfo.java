package apikunaio.contracts.response;


import com.jayway.jsonpath.DocumentContext;

/* ObjectMapper om = new ObjectMapper();
Root root = om.readValue(myJsonString), Root.class); */
public class MyInfo {

    public String email;
    public String kunaid;
    public String sn;
    public boolean activated;

    public MyInfo() {
    }

    public MyInfo(DocumentContext jsonDocumentContext) {
        try {
            email = jsonDocumentContext.read("$['email']");
            kunaid = jsonDocumentContext.read("$['kunaid']");
            sn = jsonDocumentContext.read("$['sn']");
            activated = jsonDocumentContext.read("$['activated']");
        } catch (Exception e) {
            System.err.println("Error when tried to create " + MyInfo.class);
        }
    }
}









