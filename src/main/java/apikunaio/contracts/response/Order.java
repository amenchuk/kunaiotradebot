package apikunaio.contracts.response;

import net.minidev.json.JSONArray;

/**
 * Response body:
 * [
 * [
 * 0    100279610, # order ID
 * 1    null, # not used
 * 2    null, # not used
 * 3    'xrpuah', # market code (currency pair)
 * 4    1560089091000, # creation time (timestamp in milliseconds)
 * 5    1560089091000, # update time (timestamp in milliseconds)
 * 6    '45 .0 ', # order volume
 * 7    '-45.0', # initial order volume; if the value is <0 then a sell order; if> 0, then buy order
 * 8    'LIMIT', # order type (LIMIT or MARKET)
 * 9    null, # not used
 * 10    null, # not used
 * 11    null, # not used
 * 12    null, # not used
 * 13    'EXECUTED', # order status
 * 14    null, # not used
 * 15    null, # not used
 * 16    '14 .0 ', # order price
 * 17    '13 .0 '# average of trades per order
 * ]
 * ]
 */
public class Order {

    public int orderId;
    public String traidPair;
    public long createTime;
    public long updatedTime;
    public double orderVolume;
    public double initialOrderVolume;
    public String orderType;
    public String orderStatus;
    public double orderPrice;
    public double avgOrderPrice;

    public Order(JSONArray orderData) {
        try {
            orderId = Integer.parseInt(orderData.get(0).toString());
            traidPair = orderData.get(3).toString();
            createTime = Long.parseLong(orderData.get(4).toString());
            updatedTime = Long.parseLong(orderData.get(5).toString());
            orderVolume = Double.parseDouble(orderData.get(6).toString());
            initialOrderVolume = Double.parseDouble(orderData.get(7).toString());
            orderType = orderData.get(8).toString();
            orderStatus = orderData.get(13).toString();
            orderPrice = Double.parseDouble(orderData.get(16) != null ? orderData.get(16).toString() : "0.0");
            avgOrderPrice = Double.parseDouble(orderData.get(17).toString());
        } catch (Exception e) {
            System.err.println("Error when tried to create " + OrdersInfo.class);
        }
    }
}
