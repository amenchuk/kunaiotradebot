package apikunaio.contracts.response;

import net.minidev.json.JSONArray;

/**
 * Response body:
 * [
 * [
 * "exchange",
 * "USD", # currency symbol
 * 1208370, # full account balance
 * null, # not used
 * 1208370 # available funds
 * ],
 * [
 * "exchange",
 * "KUN",
 * 800000,
 * null,
 * 760,
 * 0
 * ]
 * ]
 */
public class WalletBalance {

    public String currencySymbol;
    public double accountBalance;
    public double availableFunds;

    public WalletBalance(JSONArray walletData) {
        try {
            currencySymbol = walletData.get(1).toString();
            accountBalance = Double.parseDouble(walletData.get(2).toString());
            availableFunds = Double.parseDouble(walletData.get(4).toString());
        } catch (Exception e) {
            System.err.println("Error when tried to create " + WalletBalance.class);
        }
    }

}