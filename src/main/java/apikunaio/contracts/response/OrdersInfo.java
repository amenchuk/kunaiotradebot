package apikunaio.contracts.response;

import com.jayway.jsonpath.DocumentContext;
import net.minidev.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class OrdersInfo {

    private final List<Order> orders = new ArrayList<>();

    public OrdersInfo(DocumentContext jsonDocumentContext) {
        try {
            for (JSONArray subArr : (ArrayList<JSONArray>) jsonDocumentContext.json()) {
                orders.add(new Order(subArr));
            }
        } catch (ClassCastException e) {
            System.err.println("Error when tried to cast API response to JSONArray in " + OrdersInfo.class);
        }
    }

    public List<Order> getOrders() {
        return orders;
    }

    public Order getOrder(int orderId) {
        return orders.stream().filter(x -> x.orderId == orderId).findFirst().orElse(null);
    }
}
