package apikunaio.contracts.response;

import com.jayway.jsonpath.DocumentContext;
import net.minidev.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class Wallets {

    private final List<WalletBalance> walletWalletBalanceList = new ArrayList<>();

    public Wallets(DocumentContext jsonDocumentContext) {
        try {
            for (JSONArray walletData : (List<JSONArray>) jsonDocumentContext.json()) {
                walletWalletBalanceList.add(new WalletBalance(walletData));
            }
        } catch (ClassCastException e) {
            System.err.println("Error when tried to cast API response to JSONArray in " + Wallets.class);
        }
    }

    public List<WalletBalance> getWalletsBalance() {
        return walletWalletBalanceList;
    }

    public WalletBalance getWalletBalance(String currencySymbol) {
        return walletWalletBalanceList.stream().filter(x -> x.currencySymbol.equals(currencySymbol.toUpperCase())).findFirst().orElse(null);
    }

}
