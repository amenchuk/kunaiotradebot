package apikunaio.contracts.response;

import com.jayway.jsonpath.DocumentContext;

/* Response body:
[
   [
     "btcuah", # market symbol
     208001, # BID price
     11200693, # BID order book volume
     208499, # ASK price
     29.255569, # order book volume ASK
     5999, # 24 hours price change in quoted currency
     -2.8, # price change for 24 hours in percent
     208001, # last price
     11.3878, # 24 hours trading volume in base currency
     215301, # maximum price in 24 hours
     208001 # minimum price for 24 hours
   ]
]
 */
public class MarketPairInfo {

    public String marketSymbol;
    public double priceBid;
    public double orderBookVolumeBid;
    public double priceAsk;
    public double orderBookVolumeAsk;
    public double priceChange24HQuotedCurrency;
    public double priceChange24HPercents;
    public double priceLast;
    public double tradingVolume24HBaseCurrency;
    public double priceMax24H;
    public double priceMin24H;

    public MarketPairInfo() {
    }

    public MarketPairInfo(DocumentContext jsonDocumentContext) {
        try {
            marketSymbol = jsonDocumentContext.read("$[0].[0]");
            priceBid = jsonDocumentContext.read("$[0].[1]");
            orderBookVolumeBid = jsonDocumentContext.read("$[0].[2]");
            priceAsk = jsonDocumentContext.read("$[0].[3]");
            orderBookVolumeAsk = jsonDocumentContext.read("$[0].[4]");
            String priceChng24HQtdCurcStr = jsonDocumentContext.read("$[0].[5]").toString();
            priceChange24HQuotedCurrency = Double.valueOf(priceChng24HQtdCurcStr.substring(0, priceChng24HQtdCurcStr.length() > 5 ? 6 : priceChng24HQtdCurcStr.length() - 1));
            priceChange24HPercents = jsonDocumentContext.read("$[0].[6]");
            priceLast = jsonDocumentContext.read("$[0].[7]");
            tradingVolume24HBaseCurrency = jsonDocumentContext.read("$[0].[8]");
            priceMax24H = jsonDocumentContext.read("$[0].[9]");
            priceMin24H = jsonDocumentContext.read("$[0].[10]");
        } catch (Exception e) {
            System.err.println("Error when tried to create " + MarketPairInfo.class);
        }
    }

}
