package apikunaio;

public class RoutesAPI {

    public static final String BASE_URL = "https://api.kuna.io";

    public static final String MY_INFO_URL = "/v3/auth/me";

    public static final String MARKET_INFO_URL = "/v3/tickers";

    public static final String BALANCE_INFO_URL = "/v3/auth/r/wallets";

    public static final String CREATE_ORDER_URL = "/v3/auth/w/order/submit";

    public static String ORDER_HISTORY_URL(String tradePairSymbol) {
        return String.format("/v3/auth/r/orders/%s/hist", tradePairSymbol);
    }

    public static String ORDER_ACTIVE_URL(String tradePairSymbol) {
        return String.format("/v3/auth/r/orders/%s", tradePairSymbol);
    }
}
