package apikunaio;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import okhttp3.*;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;

import java.io.IOException;
import java.util.HashMap;

public class BaseAPI {

    protected static String PUBLIC_KEY;
    protected static String SECRET_KEY;

    protected static DocumentContext execRequestAndGetDocumentContext(Request request) {
        DocumentContext documentContext = null;
        try {
            ResponseBody responseBody = new OkHttpClient().newBuilder().build().newCall(request).execute().body();
            documentContext = JsonPath.parse(responseBody.string());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return documentContext;
    }

    protected Request createGetRequest(String urlPath) {
        return new Request.Builder().url(RoutesAPI.BASE_URL + urlPath)
                .method("GET", null)
                .build();
    }

    protected Request createGetRequest(String urlPath, HashMap<String, String> urlParameters) {
        return new Request.Builder().url(RoutesAPI.BASE_URL + urlPath + createUrlParamLine(urlParameters))
                .method("GET", null)
                .build();
    }

    protected Request createPostRequest(String urlPath) {
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create("", mediaType);

        String kunNonce = String.valueOf(System.currentTimeMillis());
        String kunSignature = generateSignature(urlPath, kunNonce);

        return new Request.Builder()
                .url(RoutesAPI.BASE_URL + urlPath)
                .method("POST", body)
                .addHeader("accept", "application/json")
                .addHeader("content-type", "application/json")
                .addHeader("kun-nonce", kunNonce)
                .addHeader("kun-apikey", PUBLIC_KEY)
                .addHeader("kun-signature", kunSignature)
                .build();
    }

    protected Request createPostRequest(String urlPath, HashMap<String, String> urlParameters) {
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create("", mediaType);

        String kunNonce = String.valueOf(System.currentTimeMillis());
        String kunSignature = generateSignature(urlPath, kunNonce);

        return new Request.Builder()
                .url(RoutesAPI.BASE_URL + urlPath + createUrlParamLine(urlParameters))
                .method("POST", body)
                .addHeader("accept", "application/json")
                .addHeader("content-type", "application/json")
                .addHeader("kun-nonce", kunNonce)
                .addHeader("kun-apikey", PUBLIC_KEY)
                .addHeader("kun-signature", kunSignature)
                .build();
    }

    private String generateSignature(String urlPath, String kunNonce) {
        return new HmacUtils(HmacAlgorithms.HMAC_SHA_384, SECRET_KEY).hmacHex(urlPath + kunNonce);
    }

    private String createUrlParamLine(HashMap<String, String> urlParameters) {
        String paramLine = "";

        if (urlParameters != null && !urlParameters.isEmpty()) {
            StringBuilder string = new StringBuilder().append("?");
            urlParameters.entrySet().forEach(x -> string.append(x.getKey() + "=" + x.getValue() + "&"));
            paramLine = string.toString();
        }

        return paramLine.substring(0, paramLine.length() - 1).toLowerCase();
    }
}
