package apikunaio;

import apikunaio.contracts.response.*;
import okhttp3.Request;

import java.util.ArrayList;
import java.util.HashMap;

public final class TradeControllerAPI extends BaseAPI {

    public TradeControllerAPI(String publicApiKey, String secretApiKey) {
        PUBLIC_KEY = publicApiKey;
        SECRET_KEY = secretApiKey;
    }

    public MyInfo getMyInfo() {
        Request request = createPostRequest(RoutesAPI.MY_INFO_URL);
        MyInfo myInfo = new MyInfo(execRequestAndGetDocumentContext(request));

        return myInfo;
    }

    public MarketPairInfo getMarketPairInfo(String pairName) {
        HashMap<String, String> urlParameters = new HashMap<String, String>();
        urlParameters.put("symbols", pairName);

        Request request = createGetRequest(RoutesAPI.MARKET_INFO_URL, urlParameters);
        MarketPairInfo marketPairInfo = new MarketPairInfo(execRequestAndGetDocumentContext(request));

        return marketPairInfo;
    }

    public WalletBalance getBalanceOf(String currencySymb) {
        Request request = createPostRequest(RoutesAPI.BALANCE_INFO_URL);
        WalletBalance walletBalance = new Wallets(execRequestAndGetDocumentContext(request)).getWalletBalance(currencySymb);

        return walletBalance;
    }

    public Order getExecutedOrderInfo(String tradePairSymbol, int orderId) {
        Request request = createPostRequest(RoutesAPI.ORDER_HISTORY_URL(tradePairSymbol.toLowerCase()));
        Order order = new OrdersInfo(execRequestAndGetDocumentContext(request)).getOrder(orderId);

        return order;
    }

    public ArrayList<Order> getActiveOrders(String tradePairSymbol) {
        Request request = createPostRequest(RoutesAPI.ORDER_ACTIVE_URL(tradePairSymbol.toLowerCase()));
        ArrayList<Order> orders = (ArrayList<Order>) new OrdersInfo(execRequestAndGetDocumentContext(request)).getOrders();
        return orders;
    }

    public Order postBuyOrder(String tradePairSymbol, String orderType, double amountOfFiatCurrency) {
        HashMap<String, String> urlParameters = new HashMap<String, String>();
        urlParameters.put("symbol", tradePairSymbol);
        urlParameters.put("type", orderType);
        urlParameters.put("amount", String.valueOf(amountOfFiatCurrency));

        Request request = createPostRequest(RoutesAPI.CREATE_ORDER_URL, urlParameters);
        Order tradeOrder = new Order(execRequestAndGetDocumentContext(request).json());

        return tradeOrder;
    }

    public Order postSellOrder(String tradePairSymbol, String orderType, double amountOfFiatCurrency, double priceForSell) {
        HashMap<String, String> urlParameters = new HashMap<String, String>();
        urlParameters.put("symbol", tradePairSymbol);
        urlParameters.put("type", orderType);
        urlParameters.put("amount", "-" + amountOfFiatCurrency);
        urlParameters.put("price", String.valueOf(priceForSell));

        Request request = createPostRequest(RoutesAPI.CREATE_ORDER_URL, urlParameters);
        Order tradeOrder = new Order(execRequestAndGetDocumentContext(request).json());

        return tradeOrder;
    }
}
