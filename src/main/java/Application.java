import trade.business.TradeController;
import trade.strategies.TradeStrategy;
import utils.TradeSettingObject;
import utils.TradeSettings;

import java.util.*;

public class Application {

    public static void main(String[] args) {

        if(args.length<1){
            System.err.println("Point the name of property file as an argument!\n" +
                    "Example: java -jar Kuna.io.Trader-1.0.jar trade_settings.json");
        }

        TradeSettingObject settings = TradeSettings.getTradeSettings(args[0]);

        String publicApiKey = settings.publicApiKey;
        String secretApiKey = settings.secretApiKey;
        String tradePairSymbol = settings.tradePairSymbol;
        double amountOfCurrencyForTrade = settings.amountOfCurrencyForTrade;
        double percentOfDesireProfit = settings.percentOfDesireProfit; // can be from 0.01% to 100.0%. advice - not more 1.0%
        double amountOfCurrencyToOneBuyOrder = settings.amountOfCurrencyToOneBuyOrder;

        TradeController tradeController = new TradeController(amountOfCurrencyForTrade, tradePairSymbol, publicApiKey, secretApiKey);

        Map<Integer, LinkedList<Double>> tickerStorage = new HashMap<>();
        tickerStorage.put(5, new LinkedList<>());
        tickerStorage.put(15, new LinkedList<>());
        tickerStorage.put(30, new LinkedList<>());
        tickerStorage.put(60, new LinkedList<>());

        new Timer()
                .schedule(
                        new TimerTask() {
                            public void run() {
                                int currentTimePoint = Calendar.getInstance().get(Calendar.MINUTE);
                                new TradeStrategy(tradeController)
                                        .execute(amountOfCurrencyToOneBuyOrder, percentOfDesireProfit, tickerStorage, currentTimePoint);
                            }
                        },
                        getExecutionDelay(),
                        300000 /*5min in milliseconds*/);
    }

    private static long getExecutionDelay() {
        long delay = -1;
        while (delay < 0) {
            if (Calendar.getInstance().get(Calendar.MINUTE) % 5 == 0)
                delay = 0;
        }

        return delay;
    }

}
