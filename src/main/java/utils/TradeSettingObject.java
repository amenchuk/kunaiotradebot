package utils;

public class TradeSettingObject {

        public String publicApiKey;
        public String secretApiKey;
        public String tradePairSymbol;
        public double amountOfCurrencyToOneBuyOrder;
        public double percentOfDesireProfit;
        public double amountOfCurrencyForTrade;

}
