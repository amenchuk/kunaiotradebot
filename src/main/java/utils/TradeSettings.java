package utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TradeSettings {

    public static TradeSettingObject getTradeSettings(String fileName) {
        JSONObject settingsReader = getFile(fileName);

        TradeSettingObject settings = new TradeSettingObject();

        settings.publicApiKey = (String) settingsReader.get("publicApiKey");
        settings.secretApiKey = (String) settingsReader.get("secretApiKey");
        settings.tradePairSymbol = (String) settingsReader.get("tradePairSymbol");
        settings.amountOfCurrencyToOneBuyOrder = (Double) settingsReader.get("amountOfCurrencyToOneBuyOrder");
        settings.percentOfDesireProfit = (Double) settingsReader.get("percentOfDesireProfit");
        settings.amountOfCurrencyForTrade = (Double) settingsReader.get("amountOfCurrencyForTrade");

        return settings;
    }

    private static JSONObject getFile(String fileName) {
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) new JSONParser().parse(new FileReader(new File(fileName)));
        } catch (IOException | ParseException e) {
            System.err.println("[ERROR] Something went wrong during parsing 'trade_settings.json' settings file.\n\t" + e.getMessage());
            System.err.println("\tThe 'trade_settings.json' file should be as:\n" +
                    "{\n\t\"publicApiKey\":\"DTnWddwFrYKxfad2aQvBA3m76f4DJwCgVpClO7WG\",\n\t\"secretApiKey\":\"xxywA6spELxQZOsrRhxRSdJvnhbUwJweTMFx80ki\",\n\t\"tradePairSymbol\":\"xemuah\",\n\t\"amountOfCurrencyToOneBuyOrder\":3.0,\n\t\"percentOfDesireProfit\":0.5,\n\t\"amountOfCurrencyForTrade\":20.0\n}");
            System.exit(1);
        }

        return jsonObject;
    }
}

/*public class TradeSettingFile{
    public String publicApiKey;
    public String secretApiKey;
    public String tradePairSymbol;
    public double amountOfCurrencyToOneBuyOrder;
    public double percentOfDesireProfit;
    public double amountOfCurrencyForTrade;
}*/
